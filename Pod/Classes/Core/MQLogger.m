#import "MQLogger.h"

#ifdef MQ_AFLOGGER
#import "MQAFNetworkLogger.h"
#endif

@implementation MQLogger

#pragma mark Class Method
+ (MQLogger *)getLogger {
    static MQLogger *logger = nil;
    if (logger == nil) {
        logger = [[MQLogger alloc] init];
    }
    return logger;
}

+ (NSString *)levelName:(MQLoggerLevel)level {
    switch (level) {
        case MQ_TINY:
            return @"TINY";
            break;
        case MQ_DETAIL:
            return @"DETAIL";
            break;
        case MQ_ENTER:
            return @"ENTER";
            break;
        case MQ_RETURN:
            return @"RETURN";
            break;
        case MQ_INFO:
            return @"INFO";
            break;
        case MQ_DEBUG:
            return @"DEBUG";
            break;
        case MQ_WARNING:
            return @"WARN";
            break;
        case MQ_ERROR:
            return @"ERROR";
            break;
        case MQ_FATAL:
            return @"FATAL";
            break;
        default:
            return @"NOLEVE";
            break;
    }
}

#pragma mark -
#pragma mark Method init

- (id)initWithLogLevel:(MQLoggerLevelSetting)levelSetting {
    if (self = [super init]) {
        self.logLevelSetting = levelSetting;
    }
    return self;
}

- (id)init {
    return [self initWithLogLevel:MQS_MAJOR];
}

#pragma mark -
#pragma mark Method

void print() {
}

- (void)log:(NSString *)msg
  withLevel:(MQLoggerLevel)level
     inFile:(NSString *)fileName
     inLine:(int)lineNr {
    if (level > self.logLevelSetting) {
        NSLog(@"{%@:%d}[%@]%@", fileName, lineNr, [MQLogger levelName:level], msg);
    }
}

- (void)enter:(NSString *)msg
       inFile:(NSString *)fileName
       inLine:(int)lineNr {
    [self log:msg withLevel:MQ_ENTER inFile:fileName inLine:lineNr];
}

- (void)retrn:(NSString *)msg
       inFile:(NSString *)fileName
       inLine:(int)lineNr {
    [self log:msg withLevel:MQ_RETURN inFile:fileName inLine:lineNr];
}

- (void)info:(NSString *)msg
      inFile:(NSString *)fileName
      inLine:(int)lineNr {
    [self log:msg withLevel:MQ_INFO inFile:fileName inLine:lineNr];
}

- (void)debug:(NSString *)msg
       inFile:(NSString *)fileName
       inLine:(int)lineNr {
    [self log:msg withLevel:MQ_DEBUG inFile:fileName inLine:lineNr];
}

- (void)warn:(NSString *)msg
      inFile:(NSString *)fileName
      inLine:(int)lineNr {
    [self log:msg withLevel:MQ_WARNING inFile:fileName inLine:lineNr];
}

- (void)error:(NSString *)msg
       inFile:(NSString *)fileName
       inLine:(int)lineNr {
    [self log:msg withLevel:MQ_ERROR inFile:fileName inLine:lineNr];
}

#pragma mark - private method
#ifdef MQ_AFLOGGER
static AFHTTPRequestLoggerLevel TransAFHTTPRequestLoggerLevel(MQLoggerLevelSetting logLevelSetting) {
    switch (logLevelSetting) {
        case MQS_NONE:
            return AFLoggerLevelOff;
            break;
        case MQS_MINOR:
            return AFLoggerLevelError;
            break;
        case MQS_MEDIUM:
            return AFLoggerLevelWarn;
            break;
        case MQS_MAJOR:
            return AFLoggerLevelInfo;
            break;
        case MQS_ALL:
            return AFLoggerLevelDebug;
            break;
        default:
            return AFLoggerLevelInfo;
            break;
    }
}
#endif

#pragma mark - getters and setters
- (void)setLogLevelSetting:(MQLoggerLevelSetting)logLevelSetting {
#ifdef MQ_AFLOGGER
    AFHTTPRequestLoggerLevel newAFlevel = TransAFHTTPRequestLoggerLevel(logLevelSetting);
    [[MQAFNetworkLogger sharedLogger] setLevel:newAFlevel];
    if (newAFlevel == AFLoggerLevelOff) {
        [[MQAFNetworkLogger sharedLogger] stopLogging];
    } else {
        [[MQAFNetworkLogger sharedLogger] startLogging];
    }
#endif
    _logLevelSetting = logLevelSetting;
}

@end
