/**
 *  @brief 日志处理
 */
#import <Foundation/Foundation.h>

#ifndef __SELF_DEFING_CLOSELOGGER__

#define LOG(level, format, ...) [[MQLogger getLogger] log:[NSString stringWithFormat:(format), ##__VA_ARGS__] \
withLevel:level \
inFile:[[NSString stringWithUTF8String:__FILE__] lastPathComponent] \
inLine:__LINE__]
#define ENTER(format, ...) [[MQLogger getLogger] enter:[NSString stringWithFormat:(format), ##__VA_ARGS__] \
inFile:[[NSString stringWithUTF8String:__FILE__] lastPathComponent] \
inLine:__LINE__]
#define RETURN(returnValue, format, ...) [[MQLogger getLogger] retrn:[NSString stringWithFormat:(format), ##__VA_ARGS__] \
inFile:[[NSString stringWithUTF8String:__FILE__] lastPathComponent] \
inLine:__LINE__]; return (returnValue)
#define INFO(format, ...) [[MQLogger getLogger] info:[NSString stringWithFormat:(format), ##__VA_ARGS__] \
inFile:[[NSString stringWithUTF8String:__FILE__] lastPathComponent] \
inLine:__LINE__]
#define MDEBUG(format, ...) [[MQLogger getLogger] debug:[NSString stringWithFormat:(format), ##__VA_ARGS__] \
inFile:[[NSString stringWithUTF8String:__FILE__] lastPathComponent] \
inLine:__LINE__]
#define WARN(format, ...) [[MQLogger getLogger] warn:[NSString stringWithFormat:(format), ##__VA_ARGS__] \
inFile:[[NSString stringWithUTF8String:__FILE__] lastPathComponent] \
inLine:__LINE__]
#define ERROR(format, ...) [[MQLogger getLogger] error:[NSString stringWithFormat:(format), ##__VA_ARGS__] \
inFile:[[NSString stringWithUTF8String:__FILE__] lastPathComponent] \
inLine:__LINE__]

#else

#define LOG(level, format, ...)
#define ENTER(format, ...)
#define RETURN(format, ...)
#define INFO(format, ...)
#define MDEBUG(format, ...)
#define WARN(format, ...)
#define ERROR(format, ...)

#endif

typedef enum {
    MQ_TINY    = 10,
    MQ_DETAIL  = 20,
    MQ_ENTER   = 30,
    MQ_RETURN  = 31,
    MQ_INFO    = 40,
    MQ_DEBUG   = 50,
    MQ_WARNING = 60,
    MQ_ERROR   = 70,
    MQ_FATAL   = 80
}MQLoggerLevel;

typedef enum {
    MQS_ALL = 0, // should be smaller than all MQLoggerLevel
    MQS_MAJOR = 21, // should be larger than MQ_DETAIL
    MQS_MEDIUM = 35, // should be larger than MQ_RETURN
    MQS_MINOR = 55, // should be larger than MQ_DEBUG
    MQS_NONE = 1000 // should be larger than all
}MQLoggerLevelSetting;

/**
 *  @brief 日志处理
 */
@interface MQLogger : NSObject

@property (nonatomic, assign) MQLoggerLevelSetting logLevelSetting;

- (id)initWithLogLevel:(MQLoggerLevelSetting)levelSetting;
- (void)log:(NSString *)msg
  withLevel:(MQLoggerLevel)level
     inFile:(NSString *)fileName
     inLine:(int)lineNr;

- (void)enter:(NSString *)msg
       inFile:(NSString *)fileName
       inLine:(int)lineNr;
- (void)retrn:(NSString *)msg
       inFile:(NSString *)fileName
       inLine:(int)lineNr;
- (void)info:(NSString *)msg
      inFile:(NSString *)fileName
      inLine:(int)lineNr;
- (void)debug:(NSString *)msg
       inFile:(NSString *)fileName
       inLine:(int)lineNr;
- (void)warn:(NSString *)msg
      inFile:(NSString *)fileName
      inLine:(int)lineNr;
- (void)error:(NSString *)msg
       inFile:(NSString *)fileName
       inLine:(int)lineNr;

+ (MQLogger *)getLogger;
+ (NSString *)levelName:(MQLoggerLevel)level;

@end
