# MQLogger

[![CI Status](http://img.shields.io/travis/cailu/MQLogger.svg?style=flat)](https://travis-ci.org/cailu/MQLogger)
[![Version](https://img.shields.io/cocoapods/v/MQLogger.svg?style=flat)](http://cocoapods.org/pods/MQLogger)
[![License](https://img.shields.io/cocoapods/l/MQLogger.svg?style=flat)](http://cocoapods.org/pods/MQLogger)
[![Platform](https://img.shields.io/cocoapods/p/MQLogger.svg?style=flat)](http://cocoapods.org/pods/MQLogger)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MQLogger is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MQLogger"
```

## Author

cailu, louis.cai.cn@gmail.com

## License

MQLogger is available under the MIT license. See the LICENSE file for more info.
