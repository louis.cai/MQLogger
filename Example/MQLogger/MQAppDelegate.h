//
//  MQAppDelegate.h
//  MQLogger
//
//  Created by cailu on 07/07/2015.
//  Copyright (c) 2015 cailu. All rights reserved.
//

@import UIKit;

@interface MQAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
