//
//  MQViewController.m
//  MQLogger
//
//  Created by cailu on 07/07/2015.
//  Copyright (c) 2015 cailu. All rights reserved.
//

#import "AFNetworking.h"
#import "MQLogger.h"
#import "MQViewController.h"

#define LOG_LEVEL_DEF ddLogLevel

@interface MQViewController ()

@end

@implementation MQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    [MQLogger getLogger].logLevelSetting = MQS_MEDIUM;
    ENTER(@"%@", @"ENTER");
    INFO(@"%@", @"INFO");
    MDEBUG(@"%@", @"MDEBUG");
    WARN(@"%@", @"MDEBUG");
    ERROR(@"%@", @"MDEBUG");

    [[AFHTTPSessionManager manager] GET:@"http://www.baidu.com"
                             parameters:nil
                               progress:nil
                                success:nil
                                failure:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
