//
//  main.m
//  MQLogger
//
//  Created by cailu on 07/07/2015.
//  Copyright (c) 2015 cailu. All rights reserved.
//

@import UIKit;
#import "MQAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MQAppDelegate class]));
    }
}
