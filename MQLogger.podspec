#
# Be sure to run `pod lib lint MQLogger.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "MQLogger"
  s.version          = "0.2.1"
  s.summary          = "MeiQi MQLogger for logger."
  s.description      = <<-DESC
                       A MQLogger with CocoaLumberjack and AFNetWorkingLogger.
                       DESC
  s.homepage         = "https://git.oschina.net/louis.cai/MQLogger"
  s.license          = 'MIT'
  s.author           = { "cailu" => "louis.cai.cn@gmail.com" }
  s.source           = { :git => "git@git.oschina.net:louis.cai/MQLogger.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

# s.resource_bundles = {'MQLogger' => ['Pod/Assets/*.png']}

  s.default_subspec = 'Core'
    s.subspec 'Core' do |core|
    core.source_files = 'Pod/Classes/Core/*.{h,m}'
    core.frameworks   = 'Foundation'
#core.dependency 'CocoaLumberjack', '~> 2.0'
  end

  s.subspec 'AFLogger' do |logger|
    logger.source_files = 'Pod/Classes/AFLogger/*.{h,m}'
    logger.frameworks   = 'Foundation'
    logger.dependency 'MQLogger/Core'
    logger.xcconfig = { 'GCC_PREPROCESSOR_DEFINITIONS' => '$(inherited) MQ_AFLOGGER=1' }
    logger.dependency 'AFNetworking', '~> 3.0'
  end
end
